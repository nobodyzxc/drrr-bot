
const fs      = require('fs');
const DrrrBot = require('./src/DrrrBot.js').DrrrBot;
const REPL    = require('repl');

const USAGE = `Usage: main.js [--cookie=value] [--room=value]`;


class DrrrBotREPL {
  constructor(opts) {
    this.PROMPT = 'DrrrBot > '
    this.HELP = `
  Syntax: .[command] [args]

  Commands:
    .h                   : Show this help
    .q                   : Quit the REPL
    .c [cmt]             : Send a comment
    .users               : Show the user list
    .showc               : Show comments
    .stopc               : Stop showing comments
    .config              : Print config file
    .spam                : Give em hell
    .toggle [mod]        : Toggle mod
    .reload [mod]        : Reload a mod
    .rights [name] [lvl] : Set someone's tripcode right level
    .kick         [name] : Kick some ass
    .ban          [name] : Ban some ass
    .uname        [name] : Ban a name
    .ubname       [name] : Unban a name
    .banAndReport [name] : Ban and report some ass
`;
    
    this.bot = new DrrrBot(opts);
  };

  // Helper to define commands
  define_command(key, func) {
    this.repl.defineCommand(key, {
      action(args) {
        this.clearBufferedCommand();
        func(args);
        this.displayPrompt();
      }
    })
  }
  
  find_user_by_name(name) {
    return Object.values(this.bot.users).find( user => 
      user.name === name
    );
  }

  async init() {
    const that = this;
    
    await this.bot.init();

    this.repl = REPL.start({
      prompt: this.PROMPT
    });

    this.define_command( 'h',      ()   => console.log(this.HELP)        );
    this.define_command( 'q',      ()   => process.exit()                ); 
    this.define_command( 'stopc',  ()   => this.stop_comments()          );
    this.define_command( 'config', ()   => console.log(this.bot.config)  );
    this.define_command( 'c',      msg  => this.bot.message(msg)         );
    this.define_command( 'yt',     id   => this.bot.mod.pls.share_yt(id) );
    this.define_command( 'reload', name => this.bot.reload_mod(name)     );
    this.define_command( 'spam',   n    => this.bot.mod.spam.run(n)      );

    this.define_command('toggle', name => {
      this.bot.mod[name].on = !this.bot.mod[name].on;
      console.log(`Name is now ${this.bot.mod[name].on ? 'on' : 'off'}.`);
    });

    // Set the rights for tripcodes
    this.define_command('rights', args => {
      const parts = args.split(' ');
      const level = parts.pop();
      const name  = parts.join(' ');

      const user = this.find_user_by_name(name);

      if(user) {
        this.bot.config.rights[user.tripcode] = parseInt(level);
        this.bot.write_config();
      }
      else {
        console.log('No such individual or association based on the caiman islands');
      }
    });

    this.define_command('users', () =>
      Object.values(this.bot.users).forEach( user =>
        console.log(`${user.name} -> ${user.id}`)
      )
    );

    // Ban a name and add it to config
    // console.log(this.bot.users);
    this.define_command('bname', name => {
      this.bot.config.banned.push(name);
      this.bot.write_config();
    });

    // Unban a name and add it to config
    this.define_command('ubname', name => {
      // Remove all words from the array
      for(let i = 0; i < this.bot.config.banned.names.length; i++) {
        if( this.bot.config.banned.names[i] === name ) {
          this.bot.config.banned.names.splice(i, 1);
          i--;
        }
      }

      this.bot.write_config();
    });

   //Kick Command
    this.define_command('kick', name => {
      console.log( Object.keys(this.bot.users) );
      const user = this.find_user_by_name(name);
      console.log(user);

      if(user)
        this.bot.kick(user.id);
      else
        console.log('Dang, no such user!');
    });

    //Ban command
    this.define_command('ban', name => {
      const user = this.find_user_by_name(name);

      if(user)
        this.bot.ban(user.id);
      else
        console.log('U sure dis guy exists in this universe.');
    });

    //Ban and Report command
    this.define_command('banAndReport',  name => {
      const user = this.find_user_by_name(name);

      if(user)
        this.bot.banAndReprort(user.uid);
      else
        console.log('No such guy m8.');
    });

    // Dont display prompt
    this.repl.defineCommand('showc', { action() { that.show_comments(); } } );
  }

  show_comments() {
    this.bot.FLAGS.printc = true;

    this.bot.talks.slice(-10).forEach( talk =>
      this.bot.print_talk(talk)
    );
  }
  stop_comments() {
    this.bot.FLAGS.printc = false;
  }
}

function read_opts() {
  const opts = {};

  this.process.argv.slice(2).forEach( arg => {
    const parts    = arg.split('=');
    const arg_name = parts[0].substr(2);

    opts[arg_name] = parts[1];
  })

  return opts;
}

async function main() {
  const opts = read_opts();
  
  if(opts.cookie) {
    try {
      const config = JSON.parse( fs.readFileSync('./config.json') );
      config.cookie = opts.cookie;
      fs.writeFileSync('./config.json', JSON.stringify(config));
    }
    catch(err) {
      throw err;
    }
  }

  const repl = new DrrrBotREPL(opts);
  await repl.init();
}

main();

