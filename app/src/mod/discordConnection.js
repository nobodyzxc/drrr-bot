const Discord = require('discord.js');
const client = new Discord.Client();
const CONFIG_PATH = './config.json';
const fs = require('fs');


function read_config() {
  return JSON.parse(fs.readFileSync(CONFIG_PATH, 'utf8'));
}

const config = read_config();


client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
  if (msg.content === 'ping') {
    msg.reply('Pong!');
  }
});

client.login(config.discord.botKey);


const sendReport = function (name, msg, user, author, authorIcon) {

  const reportedUser = {
    id: '',
    icon: '',
    tripcode: '',
  }

  if (user == undefined) {
    reportedUser.id = '0';
    reportedUser.icon = 'zaika';
    reportedUser.tripcode = 'undefined'
  } else {
    reportedUser.id = user.id;
    reportedUser.icon = user.icon;
    reportedUser.tripcode = user.tripcode;
  }

  //ReportedUserIcon

  let icon = '';

  switch (reportedUser.icon) {
    case 'zaika':
      icon = 'https://i.imgur.com/AcPJvLy.png';
      break;
    case 'setton-2x':
      icon = 'https://i.imgur.com/UTEAFUX.png';
      break;
    case 'tanaka':
      icon = 'https://i.imgur.com/usG5irl.png';
      break;
    case 'kanra':
      icon = 'https://i.imgur.com/gbo19OE.png';
      break;
    case 'bakyura':
      icon = 'https://i.imgur.com/Ce9QiUQ.png';
      break;
    case 'bakyura-2x':
      icon = 'https://i.imgur.com/QNJP41U.png';
      break;
    case 'junsui-2x':
      icon = 'https://i.imgur.com/gSzy2O1.png';
      break;
    case 'kanra-2x':
      icon = 'https://i.imgur.com/C8m6wxa.png';
      break;
    case 'kyo-2x':
      icon = 'https://i.imgur.com/TuyrhJN.png';
      break;
    case 'rotchi-2x':
      icon = 'https://i.imgur.com/Z4fwXwr.png';
      break;
    case 'saki-2x':
      icon = 'https://i.imgur.com/C1PP0G5.png';
      break;
    case 'san-2x':
      icon = 'https://i.imgur.com/wmwgZ0i.png';
      break;
    case 'sharo-2x':
      icon = 'https://i.imgur.com/GETWym2.png';
      break;
    case 'kuromu-2x':
      icon = 'https://i.imgur.com/mmBH3nS.png';
      break;
    case 'gaki-2x':
      icon = 'https://i.imgur.com/VAca8YM.png';
      break;
    case 'tanaka-2x':
      icon = 'https://i.imgur.com/jnkudNs.png';
      break;
    case 'ya-2x':
      icon = 'https://i.imgur.com/weue7Db.png';
      break;
    case 'kakka':
      icon = 'https://i.imgur.com/2OIVcEo.png';
      break;
    case 'gg':
      icon = 'https://i.imgur.com/gCp2vMG.png';
      break;
    case 'eight':
      icon = 'https://i.imgur.com/rnosy6B.png';
      break;
    case 'setton':
      icon = 'https://i.imgur.com/n0KUBh3.png';
      break;
    case 'zawa':
      icon = 'https://i.imgur.com/vCUXLHe.png';
      break;
    case 'zaika-2x':
      icon = 'https://i.imgur.com/61mbj3F.png';
      break;

  }

  //AuthorIcon
  let authorIcon2 = '';
  let color = '';

  switch (authorIcon) {
    case 'zaika':
      authorIcon2 = 'https://i.imgur.com/AcPJvLy.png';
      color = '#ab1d19';
      break;
    case 'setton-2x':
      authorIcon2 = 'https://i.imgur.com/UTEAFUX.png';
      color = '#8d8f9a';
      break;
    case 'tanaka':
      authorIcon2 = 'https://i.imgur.com/usG5irl.png';
      color = '#7ec5f9';
      break;
    case 'kanra':
      authorIcon2 = 'https://i.imgur.com/gbo19OE.png';
      color = '#fda500';
      break;
    case 'bakyura':
      authorIcon2 = 'https://i.imgur.com/Ce9QiUQ.png';
      color = '#7fdf20';
      break;
    case 'bakyura-2x':
      authorIcon2 = 'https://i.imgur.com/QNJP41U.png';
      color = '#9fcc49';
      break;
    case 'junsui-2x':
      authorIcon2 = 'https://i.imgur.com/gSzy2O1.png';
      color = '#ad7f36';
      break;
    case 'kanra-2x':
      authorIcon2 = 'https://i.imgur.com/C8m6wxa.png';
      color = '#fcae43';
      break;
    case 'kyo-2x':
      authorIcon2 = 'https://i.imgur.com/TuyrhJN.png';
      color = '#22bdeb';
      break;
    case 'rotchi-2x':
      authorIcon2 = 'https://i.imgur.com/Z4fwXwr.png';
      color = '#e8ef31';
      break;
    case 'saki-2x':
      authorIcon2 = 'https://i.imgur.com/C1PP0G5.png';
      break;
    case 'san-2x':
      authorIcon2 = 'https://i.imgur.com/wmwgZ0i.png';
      color = '#302ca7';
      break;
    case 'sharo-2x':
      authorIcon2 = 'https://i.imgur.com/GETWym2.png';
      color = '#71bee6';
      break;
    case 'kuromu-2x':
      authorIcon2 = 'https://i.imgur.com/mmBH3nS.png';
      color = '#c311d8';
      break;
    case 'gaki-2x':
      authorIcon2 = 'https://i.imgur.com/VAca8YM.png';
      color = '#2db432';
      break;
    case 'tanaka-2x':
      authorIcon2 = 'https://i.imgur.com/jnkudNs.png';
      color = '#3e7ab5';
      break;
    case 'ya-2x':
      authorIcon2 = 'https://i.imgur.com/weue7Db.png';
      color = '#77787d';
      break;
    case 'kakka':
      authorIcon2 = 'https://i.imgur.com/2OIVcEo.png';
      color = '#797130';
      break;
    case 'gg':
      authorIcon2 = 'https://i.imgur.com/gCp2vMG.png';
      color = '#cf629b';
      break;
    case 'eight':
      authorIcon2 = 'https://i.imgur.com/rnosy6B.png';
      color = '#9334ad';
      break;
    case 'setton':
      authorIcon2 = 'https://i.imgur.com/n0KUBh3.png';
      color = '#000000';
      break;
    case 'zawa':
      authorIcon2 = 'https://i.imgur.com/vCUXLHe.png';
      color = '#59a259';
      break;
    case 'zaika-2x':
      authorIcon2 = 'https://i.imgur.com/61mbj3F.png';
      color = '#c02e38';
      break;

  }


  const embed = new Discord.RichEmbed()

    .setAuthor("Author: " + author, authorIcon2)

    .setColor(color)
    .setThumbnail(icon)


    .setTimestamp()
    .setURL("https://i.imgur.com/vR6RgDt.png")
    .addField("Reported User:", name)
    .addField("ID:", reportedUser.id)
    .addField("Icon:", reportedUser.icon)
    .addField("Tripcode:", reportedUser.tripcode)

    .addField("Reason:", msg, true)

    .addField("From:", "Drrr.com", false);


  client.channels.get(config.reportChannelID).send({
    embed
  });

}

module.exports.sendReport = sendReport;