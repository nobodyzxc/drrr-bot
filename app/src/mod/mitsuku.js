
const puppeteer = require('puppeteer');
const CONFIG_PATH = './config.json';
const fs        = require('fs');


function read_config() {
    return JSON.parse( fs.readFileSync( CONFIG_PATH, 'utf8' ) );
}

const config = read_config();

/*
 * Picture example: <image>Image url</image> text
 * text <image>url<image>
 * Button example: <button><text>msg</text><url>url</url></button>
 */

exports.default = class Mitsuku {
  constructor(bot) {
    this.HOST = 'https://www.pandorabots.com/mitsuku/';
   
    this.bot  = bot;
    this.is_init = false;
  }

  async init() {
    await this.create_page();
    await this.connect();
    await this.catch_events();
  }

  async create_page() {
    this.browser = await puppeteer.launch({ args: ['--no-sandbox'] });
    this.page    = await this.browser.newPage();
  }

  async connect() {
    const btn   = '.pb-widget__description__chat-now__button';
    const input = '.pb-widget__input__message__input';

    await this.page.goto(this.HOST);

    await this.page.waitFor(btn);
    await this.page.click(btn);

    await this.page.waitFor(input);
  }

  async catch_events() {
    await this.page.on('response', async res => {
      const text = await res.text();
      
      try {
        const json = JSON.parse(text);

        if( json.responses )
          await this.process_responses(json.responses);
      }
      catch(err) {
      }
    });
  }

  sanitize(msg) {
    
    let message  = msg.trim().replace(/[!\/]/g, '').replace(/Mitsuku|mitsuku/g, 'Nagisa');
    message = msg.trim().replace(/[!\/]/g, '').replace(/Mousebreaker|mousebreaker/g, 'Exii and Ms. Roboto');
    message = msg.trim().replace(/[!\/]/g, '').replace(/Steve Worswick|steve worswick/g, 'Exii and Ms. Roboto');
    message = msg.trim().replace(/[!\/]/g, '').replace(/Square Bear|squrare bear/g, 'TeamNagisa');
    message = msg.trim().replace(/[!\/]/g, '').replace(/Mitsuku|mitsuku/g, config.name);
    return message;
    
  }

  async process_responses(responses) {
  // Button example: <button><text>msg</text><url>url</url></button>
    const msg = responses[0];
    console.log('MITSUKU', msg);

    const regex = {
      img: /<image>(.+?)<\/image>/,
      btn: /<button><text>(.+?)<\/text><url>(.+?)<\/url><\/button>/
    };

    if(msg[0] === '<') {
      let _msg, url;

      if(/<image>/.test(msg)) {
        url  = msg.match(regex.img)[1];
        _msg = msg.replace(/<image>(.+?)<\/image>/, '');
      }
      else if(regex.btn.test(msg)) {
        const match = msg.match(regex.btn);
        _msg = match[1];
        url  = match[2];
      }

      this.bot.message( this.sanitize(_msg));
    }
    else {
      this.bot.message( this.sanitize(msg));
    }
  }

  async input(msg) {
    if( !this.is_init ) {
      await this.init();
      this.is_init = true;
    }

    const _msg = msg + String.fromCharCode(13);
    await this.page.type('.pb-widget__input__message__input', _msg);
  }
}

