
exports.default = class Spam {
  constructor(bot) {
    this.bot = bot;
    this.on  = true;
  }

  rand_unicode() {
    return String.fromCharCode('0x' + Math.floor(Math.random()*1000) );
  }

  rand_alphabet() {
    const A = 'abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWYZ0123456789_ '.split('');

    return A[Math.floor(Math.random()*A.length)];
  }

  msg(len) {
    return Array(len).fill(0).map( _ => this.rand_alphabet() ).join('');
  }

  async run(n) {
    const msg_n = parseInt(n);

    for(let i = 0; i < msg_n; i++) {
      const s   = Math.floor( Math.random()*500 ) + 100;
      const len = Math.floor( Math.random()*40 );
      const msg = this.msg(len).split('').map(_=>Math.random()<0.3?' ':_).join('');

      setTimeout( () => this.bot.message(msg), 400*i );
      console.log( new Date().getTime() );
    }
  }
}

