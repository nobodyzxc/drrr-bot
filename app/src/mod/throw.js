
exports.default = class Throw {
  constructor(bot) {
    this.on  = true;
    this.q   = [];
    this.bot = bot;
  }

  async boomerang(talk) {
    const s = Math.floor(Math.random()*100);
    const parts = talk.msg.split(' ');
    const obj = parts.length > 1 ? parts.slice(1).join(' ') : 'boomerang';

    this.q.push(true);

    await this.bot.me(`> ${talk.name} threw a ${obj}!`);

    setTimeout( () => {
      this.q.shift();
      this.bot.me(`> The ${obj} hit ${talk.name} back! It took ${s}s!`);
    }, s*1000);
  }

  run(talk) {
    if(this.q.length < 3)
      this.boomerang(talk);
  }
}

