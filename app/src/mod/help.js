
exports.default = class Help {
  constructor(bot) {
    this.on  = true;
    this.bot = bot;

    this.cmd = {
      'gif'  : 'Usage: !gif [keyword]. Posts a random gif',
      'pls'  : 'Usage: !pls [youtube id]. Plays a youtube video.',
      'plss' : 'Usage: !plss [youtube query]: Plays the first result of the query.',
      'question': 'Usage: !question [query]: Answers an existential doubt.',
      'throw': 'Usage: !throw. Throws a boomerang.',
      'translate': 'Usage: !translate from_lang to_lang message. Translates a sentence.'
    }
  }

  run(cmd = 'thefuckcmd') {
    if(this.cmd[cmd])
     this.bot.message_URL('Commands:', 'http://akatsukidev.de/Nagisa/nagisa.html');
    else
    this.bot.message_URL('Commands:', 'http://akatsukidev.de/Nagisa/nagisa.html');
  }
  
  async discord(){
    this.bot.message_URL('Our Discord Server:', 'https://discord.gg/s3yCs7X'); 
  }
}
