
const request = require('request');

exports.default = class Pls {
  constructor(bot) {
    this.on   = true;
    this.busy = false;
    this.bot  = bot;
  }

  // Set the resource busy and set an unlock timeout in case of error
  set_busy() {
    const timeout = 10000; // 10 secs

    this.busy = true;
    setTimeout( () => this.busy = false, timeout);
  }

  async share_yt(yt_id) {
    console.log('SHARE YT:', yt_id);

    this.set_busy();
    const url = 'https://www.youtube.com/watch?v='+yt_id;

    request.get({
      url: 'http://michaelbelgium.me/ytconverter/convert.php?youtubelink=' + url
    }, (res, data, err) => {
      try {
        const json = JSON.parse(data.body);

        if(!json.error) {
          this.busy = false;
          this.bot.share(json.file, json.title);
        }
        else {
          console.log(json);
          throw 'smth';
        }
      }
      catch(err) {
        console.log('Well, fuck.');
      }
    });
  }

  async search_yt(q_str) {
    if(this.busy) return;
    console.log('SEARCH YT:', q_str);

    q_str = q_str.split(' ').join('+');

    request.get({
      url: 'http://michaelbelgium.me/ytconverter/search.php?q=' + q_str
    }, (res, data, err) => {
      try {
        const json = JSON.parse(data.body);

        if(!json.error && json.results.length) {
          this.share_yt(json.results[0].id);
        }
        else {
          throw 'smth';
        }
      }
      catch(err) {
        console.log('Well, fuck.');
      }
    });
  }
}

